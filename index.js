//console.log("Hello Monday!")
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon:  ['Pickachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friend: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty'],
	},
	talk: function() {
		console.log("Pikachu! I choose you!");
	}
			
	};

console.log(trainer);

console.log("Result of dot notation: ");
console.log(trainer.name);
console.log("Result of square bracket notation: ")
console.log(trainer['pokemon']);
console.log("Result of talk method");
trainer.talk()

function Pokemon(name, level, health, attack, tackle, fainted){
	this.name = name;
	this.level = level;
	this.health = this.level * 2;
	this.attack = this.level;
	this.tackle = function(targetPokemon){
		   targetPokemon.health = targetPokemon.health - this.attack 
		 console.log(this.name + " tackled " + targetPokemon.name)
		 console.log(targetPokemon.name + " health is now reduced to " + targetPokemon.health)
		   if(targetPokemon.health <= 0) targetPokemon.fainted()
	}
	this.fainted = function(){
		console.log( this.name + " has fainted")
	}
	
};

let pokemon1 = new Pokemon('Pikachu', 12);
console.log(pokemon1);

let pokemon2 = new Pokemon('Geodude', 8);
console.log(pokemon2);

let pokemon3 = new Pokemon('Mewtwo', 100);
console.log(pokemon3);

 
 pokemon2.tackle(pokemon1);
 console.log(pokemon1);

 pokemon3.tackle(pokemon2);
 console.log(pokemon2);
 

